'use strict';

/**
 * @ngdoc function
 * @name mercuryTestApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the mercuryTestApp
 */
angular.module('mercuryTestApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
