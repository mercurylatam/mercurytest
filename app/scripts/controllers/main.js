'use strict';

/**
 * @ngdoc function
 * @name mercuryTestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the mercuryTestApp
 */
angular.module('mercuryTestApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
